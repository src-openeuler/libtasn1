Summary:	Libtasn1 is a ASN.1 parsing library
Name:		libtasn1
Version:	4.20.0
Release:	1

# The libtasn1 library is LGPLv2+, utilities are GPLv3+
License:	GPL-3.0-or-later AND LGPL-2.1-or-later
URL:		https://www.gnu.org/software/libtasn1/
Source0:	https://ftp.gnu.org/gnu/libtasn1/%{name}-%{version}.tar.gz

Patch0:         fix-memleaks-in-asn1-arrat2tree.patch

BuildRequires:	gcc, autoconf, automake, libtool, gnupg2, bison, pkgconfig, help2man
# when autoconf >= 2.71, the command autoreconf need gtk-doc package
BuildRequires:  gtk-doc
%ifarch %{valgrind_arches}
BuildRequires:  valgrind-devel
%endif
Provides: bundled(gnulib) = 20130324
Provides: %{name}-tools = %{version}-%{release}
Obsoletes: %{name}-tools < %{version}-%{release}

%description
Libtasn1 is the ASN.1 library used by GnuTLS, p11-kit and some other packages.
The goal of this implementation is to be highly portable, and only require an
ANSI C99 platform.This library provides Abstract Syntax Notation One (ASN.1,
as specified by the X.680 ITU-T recommendation) parsing and structures management,
and Distinguished Encoding Rules (DER, as per X.690) encoding and decoding functions.

%package devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description devel
This package contains header files and so files for development.

%package_help

%prep
%autosetup -p1

%build
autoreconf -vfi
%configure --disable-static --disable-silent-rules
%make_build

%install
%make_install
%delete_la
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%check
%make_build check

%files
%license COPYING*
%doc AUTHORS NEWS README.md THANKS
%{_bindir}/asn1*
%{_libdir}/*.so.6*

%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/*

%files help
%doc doc/TODO
%{_mandir}/man1/asn1*
%{_mandir}/man3/*asn1*
%{_infodir}/*.info.*

%changelog
* Fri Feb 07 2025 Funda Wang <fundawang@yeah.net> - 4.20.0-1
- update to 4.20.0

* Sun Aug 25 2024 Funda Wang <fundawang@yeah.net> - 4.19.0-2
- cleanup spec

* Thu Jul 13 2023 yixiangzhike <yixiangzhike007@163.com> - 4.19.0-1
- update to 4.19.0

* Tue Oct 25 2022 yixiangzhike <yixiangzhike007@163.com> - 4.17.0-3
- fix CVE-2021-46848

* Sat May 28 2022 yixiangzhike <yixiangzhike007@163.com> - 4.17.0-2
- fix fuzz issues

* Fri Dec 24 2021 yixiangzhike <yixiangzhike007@163.com> - 4.17.0-1
- update to 4.17.0

* Tue Sep 8 2020 wangchen <wangchen137@huawei.com> - 4.16.0-2
- modify the URL of Source

* Sat Jul 25 2020 linwei <linwei54@huawei.com> - 4.16.0-1
- update libtasn1 to 4.16.0

* Sat Jun 6 2020 lirui <lirui130@huawei.com> - 4.13-9
- fix fuzz issues

* Sun Mar 29 2020 whoisxxx <zhangxuzhou4@huawei.com> - 4.13-8
- Valgrind support specific arches 

* Fri Mar 20 2020 wangye <wangye54@huawei.com> - 4.13-7
- Fix CVE-2018-1000654

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.13-6
- simplify functions

* Fri Sep 06 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.13-5
- Package init
